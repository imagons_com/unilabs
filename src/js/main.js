/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-unused-vars */

// Import styles
import '../sass/default/main.scss';


// Import all images
function importAll(require) {
  return require.keys().reduce((acc, next) => {
    acc[next.replace('../', '')] = require(next);
    return acc;
  }, {});
}

const images = importAll(
  require.context('../img', false, /\.(png|jpe?g|svg)$/)
);

/* 
**********
Responsive menu opening 
**********
*/
const el = {
  button: $('.toggle-sidebar'),
  respNav: $('.responsive-nav'),
};
const page = $('html');
const arrEl = Object.values(el);

el.button.on('click', (e) => {
  arrEl.forEach((item) => {
    item.toggleClass('opened');
  });
  page.toggleClass('noscroll');
});

/* 
**********
Sticky menu
**********
*/
const headerEl = $('header');

window.addEventListener('scroll', () => {
  const pageOffset = window.pageYOffset;

  if (pageOffset > 0) {
    headerEl.addClass('smaller');
  } else {
    headerEl.removeClass('smaller');
  }
});

/* 
  HIDE HEADER ON SCROLL
*/

let prevScrollpos = window.pageYOffset;
let stickyOffset = 150;

window.onscroll = function () {
  let currentScrollPos = window.pageYOffset;

  if (prevScrollpos > currentScrollPos || currentScrollPos < stickyOffset) {
    headerEl.css('top', '0');
  } else {
    headerEl.css('top', '-80px');
  }
  prevScrollpos = currentScrollPos;
};

